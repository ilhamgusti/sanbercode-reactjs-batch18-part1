/**
 * SOAL 1
 * Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax `while`. 
 * Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur. 
 * Jangan lupa tampilkan di console juga judul ‘LOOPING PERTAMA’ dan ‘LOOPING KEDUA’.”
 */

/**
 * Jawaban Soal 1
 */
var i = 1;
console.log("Looping Pertama")
while (i <= 20) {
    console.log(`${i} - I love coding`)
    i++
}
var j = 21;
console.log("Looping Kedua")
while (j--) {
    console.log(`${j} - I will become a frontend developer`)
}


/**
 * soal 2
 * Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax for. Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu:
 * 
 * SYARAT:
 * A. Jika angka ganjil maka tampilkan Santai
 * B. Jika angka genap maka tampilkan Berkualitas
 * C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
*/

/**
 * Jawaban Soal 2
 */
for (k = 1; k <= 20; k++){
    if (k % 2 && k % 3 !== 0) {
        console.log(`${k} - Santai`);
    } else if(k%2 === 0) {
        console.log(`${k} - Berkualitas`)
    }
    if (k % 3 === 0) {
        if(k % 2 !== 0)
            console.log(`${k} - I love coding`)
    }
}

/**
 * soal 3
 * Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7. 
 * Looping boleh menggunakan syntax apa pun (while, for, do while).
*/

/**
 * Jawaban Soal 3
 */
var pager = '';
var baris = 1 ;
while (baris <= 7){
    pager +='#'
    console.log(pager)
    baris++
}

/**
 * Soal 4
 * buatlah variabel seperti di bawah ini
 * 
 * var kalimat="saya sangat senang belajar javascript"
 * ubah kalimat diatas menjadi array dengan output seperti di bawah ini
 * 
 * ["saya", "sangat", "senang", "belajar", "javascript"] 
 * 
*/

/**
 * Jawaban Soal 4
 */
var kalimat = "saya sangat senang belajar javascript";
console.log(kalimat.split(' '))

/**
 * soal 5
 * buatlah variabel seperti di bawah ini
 * 
 * var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
 * urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
 * 
 * 1. Mangga
 * 2. Apel
 * 3. Anggur
 * 4. Semangka
 * 5. Jeruk
 * 
*/
/**
 * Jawaban Soal 5
 */
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

// sorting
var done = false;
  while (!done) {
    done = true;
    for (var i = 1; i < daftarBuah.length; i += 1) {
      if (daftarBuah[i - 1] > daftarBuah[i]) {
        done = false;
        var tmp = daftarBuah[i - 1];
        daftarBuah[i - 1] = daftarBuah[i];
        daftarBuah[i] = tmp;
      }
    }
}

// looping console log
for (var index = 0; index < daftarBuah.length; index++) {
  console.log(daftarBuah[index]);
}
