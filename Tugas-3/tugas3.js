/** SOAL 1 */
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

const hasil = `${kataPertama} ${kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1)} ${kataKetiga} ${kataKeempat.toUpperCase()}`;

console.log(hasil)


/** 
 * SOAL 2 
 * 
 * buatlah variabel-variabel seperti di bawah ini
 * 
 *   var kataPertama = "1";
 *   var kataKedua = "2";
 *   var kataKetiga = "4";
 *   var kataKeempat = "5";
 * 
 * ubah lah variabel diatas ke dalam integer dan lakukan jumlahkan semua variabel dan tampilkan dalam output
*/

/**
 * Jawaban Soal 2
 */
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var hasilSoal2 = +kataPertama + +kataKedua + +kataKetiga + +kataKeempat;

console.log(hasilSoal2)

/** SOAL 3 
 * 
 *  EXPECT OUTPUT:
 *  Kata Pertama: wah
 *  Kata Kedua: javascript
 *  Kata Ketiga: itu
 *  Kata Keempat: keren
 *  Kata Kelima: sekali
*/

var kalimat = 'wah javascript itu keren sekali'; 
/**
 * Jawaban Soal 3
 */
var kata = kalimat.split(' ');
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kata[1]; // do your own! 
var kataKetiga = kata[2]; // do your own! 
var kataKeempat = kata[3]; // do your own! 
var kataKelima = kata[4]; // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);


/** 
 * SOAL 4
 * buatlah variabel seperti di bawah ini
 * 
 *    var nilai;
 * 
 * pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
 * 
 *    nilai >= 80 indeksnya A
 *    nilai >= 70 dan nilai < 80 indeksnya B
 *    nilai >= 60 dan nilai < 70 indeksnya c
 *    nilai >= 50 dan nilai < 60 indeksnya D
 *    nilai < 50 indeksnya E
 * 
*/

/**
 *  Jawaban Soal 4
 * 
 * nb: diusahakan selalu return nilai terburuk dulu (clean code)
 * https://stackoverflow.com/questions/53115008/why-is-good-to-check-for-false-first-in-an-if-statement
 *
*/

var nilai;
nilai = 75;
        
if (nilai < 50) {
    console.log("indeksnya E");
}
else if (nilai >= 70 && nilai < 80) {
    console.log("indeksnya B");
}
else if (nilai >= 60 && nilai < 70) {
    console.log("indeksnya C");
}
else if (nilai >= 50 && nilai < 60) {
    console.log("indeksnya C");
}
else {
    console.log("indeksnya A");
}


/**
 * soal 5
 * 
 * buatlah variabel seperti di bawah ini
 * 
 *      var tanggal = 22;
 *      var bulan = 7;
 *      var tahun = 2020;
 * 
 * ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda 
 * dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 
 * (isi di sesuaikan dengan tanggal lahir masing-masing)
 */

 /**
  * Jawaban Soal 5
  */
var tanggal = 10;
var bulan = 8;
var tahun = 1998;

switch (bulan) {
    case 1:
        bulan = "Januari";
        break;
    case 2:
        bulan = "Februari";
        break;
    case 3:
        bulan = "Maret";
        break;
    case 4:
        bulan = "April";
        break;
    case 5:
        bulan = "Mei";
        break;
    case 6:
        bulan = "Juni";
        break;
    case 7:
        bulan = "Juli";
        break;
    case 8:
        bulan = "Agustus";
        break;
    case 9:
        bulan = "September";
        break;
    case 10:
        bulan = "Oktober";
        break;
    case 11:
        bulan = "November";
        break;
    case 12:
        bulan = "Desember";
        break;
    default:
        throw Error('harap input bulan dalam angka');
}

console.log(`${tanggal} ${bulan} ${tahun}`)