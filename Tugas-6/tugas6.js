/**
 * Soal 1
 * buatlah fungsi menggunakan arrow function luas lingkaran dan keliling lingkaran dengan arrow function lalu gunakan let (di dalam fungsi nya saja) dan const di dalam soal ini
 * 
*/

 /**
  * Jawaban Soal 1
  *  rumus keliling lingkaran = `2 x phi x r` ATAU `phi x diameter`. dimana `phi` sama dengan `3.14` ATAU `22/7`
*/
const kelilingLingkaran = (diameter) => {
    let phi = 3.14;

    return phi * diameter;

}
  
console.log(kelilingLingkaran(100));


/**
 * Soal 2
 * buatlah variable seperti di bawah ini:
 * 
 * let kalimat = ""
 * buatlah fungsi menambahkan kata di kalimat dan gunakan penambahan kata tersebut dengan template literal, berikut ini kata kata yang mesti di tambahkan.
 * 
 *  - saya
 *  - adalah
 *  - seorang
 *  - frontend
 *  - developer
 * 
 * perlu di ingat ini fungsi menambahkan perkata, jadi fungsi ini perlu di panggil beberapa kali untuk menghasil kan kalimat (kalo dengan kasus di atas berarti di panggil 5 kali)
 * 
*/

 /**
  * Jawaban Soal 2
  * 
*/
let kalimat = "";

const sentenceMaker = (word) => {
    return kalimat += ` ${word}`;
}

sentenceMaker("saya")
sentenceMaker("adalah")
sentenceMaker("seorang")
sentenceMaker("frontend")
sentenceMaker("developer")

console.log(kalimat)


/**
 * Soal 3
 * return dalam fungsi di bawah ini masih menggunakan object literal dalam ES5, ubahlah menjadi bentuk yang lebih sederhana di ES6.
 * 
 * const newFunction = function literal(firstName, lastName){
 *   return {
 *     firstName: firstName,
 *     lastName: lastName,
 *     fullName: function(){
 *       console.log(firstName + " " + lastName)
 *       return 
 *     }
 *   }
 * }
 *  
 * Driver Code 
 * newFunction("William", "Imoh").fullName() 
 * 
*/

/**
 * Jawaban Soal 3
 *  untuk fungsi di dalam object literal sebaiknya menghindari penggunaan arrow function. https://dmitripavlutin.com/when-not-to-use-arrow-functions-in-javascript/
 */

const newFunction = (firstName, lastName) => ({
    firstName: firstName,
    lastName: lastName,
    fullName() {
        console.log(`${this.firstName} ${this.lastName}`);
        return;
    }
})
 
newFunction("William", "Imoh").fullName();


/**
 * Soal 4
 * Diberikan sebuah objek sebagai berikut:
 * 
 * const newObject = {
 *   firstName: "Harry",
 *   lastName: "Potter Holt",
 *   destination: "Hogwarts React Conf",
 *   occupation: "Deve-wizard Avocado",
 *   spell: "Vimulus Renderus!!!"
 * }
 * dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
 * 
 * const firstName = newObject.firstName;
 * const lastName = newObject.lastName;
 * const destination = newObject.destination;
 * const occupation = newObject.occupation;
 * Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
 * 
 * // Driver code
 * console.log(firstName, lastName, destination, occupation)
 */

 /**
  * Jawaban Soal 4
  */
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
};

// destructuring
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation);

/**
 * Soal 5
 * Kombinasikan dua array berikut menggunakan array spreading ES6
 * 
 * const west = ["Will", "Chris", "Sam", "Holly"]
 * const east = ["Gill", "Brian", "Noel", "Maggie"]
 * const combined = west.concat(east)
 * 
 * Driver Code
 * console.log(combined)
 * 
 */

 /**
  * Jawaban Soal 5
  */
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
console.log(combined);