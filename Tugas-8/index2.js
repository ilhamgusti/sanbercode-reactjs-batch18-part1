var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
const time = 10000;
function execute(time,index){
    readBooksPromise(time,books[index]).then(remainingTime =>{
        if(remainingTime){
            execute(remainingTime,index++);
        };
    });
};

execute(time,0);