
// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function execute(time,index){
    readBooks(time,books[index],remainingTime =>{
        if(remainingTime){
            execute(remainingTime,index++);
        };
    });
};

execute(10000,0);
