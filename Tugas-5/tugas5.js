/**
 * soal 1
 * Tulislah sebuah function dengan nama halo() yang mengembalikan nilai “Halo Sanbers!” yang kemudian dapat ditampilkan di console.
 * console.log(halo()) // "Halo Sanbers!" 
 */

 /**
  * Jawaban Soal 1
  */
function halo() {
    return "Halo Sanbers!";
}

console.log(halo());


/**
 * soal 2
 * Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter yang di kirim.
 *  
 * var num1 = 12
 * var num2 = 4
 *  
 * var hasilKali = kalikan(num1, num2)
 * console.log(hasilKali) // 48
 * 
 */

 /**
  * Jawaban Soal 2
  * 
  */

var num1 = 12;
var num2 = 4;
function kalikan(num1, num2){
    return num1 * num2;
}
  
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);


/**
 * Soal 3
 * 
 * Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: “Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!”
 *  
 * var name = "John"
 * var age = 30
 * var address = "Jalan belum jadi"
 * var hobby = "Gaming"
 *  
 * var perkenalan = introduce(name, age, address, hobby)
 * console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 
 * 
 */

/**
 * Jawaban Soal 3
 */
var name = "John";
var age = 30;
var address = "Jalan Belum jadi";
var hobby = "Gaming";

function introduce(name,age,address,hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
}

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

/**
 * Soal 4
 * ubahlah array di bawah ini menjadi object dengan property nama, jenis kelamin, hobi dan tahun lahir (var arrayDaftarPeserta harus di olah menjadi object)
 * 
 * var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
 * 
 */

 /**
  * Jawaban Soal 4
  * 
  */
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var objPeserta = {};

objPeserta.nama = arrayDaftarPeserta[0];
objPeserta.jenis_kelamin = arrayDaftarPeserta[1];
objPeserta.hobi = arrayDaftarPeserta[2];
objPeserta.tahun_lahir = arrayDaftarPeserta[3];

console.log(objPeserta)

/**
 * Soal 5
 * anda diberikan data-data buah seperti di bawah ini
 * 
 * 1.nama: strawberry
 *   warna: merah
 *   ada bijinya: tidak
 *   harga: 9000 
 * 2.nama: jeruk
 *   warna: oranye
 *   ada bijinya: ada
 *   harga: 8000
 * 3.nama: Semangka
 *   warna: Hijau & Merah
 *   ada bijinya: ada
 *   harga: 10000
 * 4.nama: Pisang
 *   warna: Kuning
 *   ada bijinya: tidak
 *   harga: 5000
 * uraikan data tersebut menjadi array of object dan munculkan data pertama
 * 
*/

/**
 * Jawaban Soal 5
 */

var listOfFruits = [
     {
        nama: 'Strawberry',
        warna: "merah",
        adaBijinya: 'tidak',
        harga:9000,
     },
     {
        nama: 'Jeruk',
        warna: "oranye",
        adaBijinya: 'ada',
        harga:8000,
     },
     {
        nama: 'Semangka',
        warna: "Hijau & Merah",
        adaBijinya: 'ada',
        harga:10000,
     },
     {
        nama: 'Pisang',
        warna: "Kuning",
        adaBijinya: 'tidak',
        harga:5000,
     },
]
 
console.log(listOfFruits[0])


/**
 * Soal 6
 * 
 * buatlah variable seperti di bawah ini
 * 
 * var dataFilm = []
 * buatlah fungsi untuk menambahkan dataFilm tersebut yang itemnya object adalah object dengan property nama, durasi , genre, tahun (fungsi menambahkan data film hanya bisa menambah satu data saja jadi jika ingin menambahkan lebih dari satu data panggil fungsi nya lebih dari satu kali). lalu tampilkan dalam output console.log(dataFilm)
 * 
*/


/**
 * Jawaban Soal 6
 */
var dataFilm = [];

function addDataFilm(nama,durasi,genre,tahun) {
    return dataFilm.push({
        nama: nama,
        durasi: durasi,
        genre: genre,
        tahun: tahun
    });
}

addDataFilm("Enola Holmes", "90 Menit", "Mystery", "2020");

console.log(dataFilm);